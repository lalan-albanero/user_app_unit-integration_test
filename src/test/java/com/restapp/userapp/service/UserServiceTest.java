package com.restapp.userapp.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.restapp.userapp.model.User;
import com.restapp.userapp.repo.UserRepo;

@SpringBootTest
public class UserServiceTest {
    @Mock
    private UserRepo userdb;
    @InjectMocks
    private UserServiceImpl userService;

    @Test
    void testAddUser() {
        User u1 = new User();
        u1.setUsername("user992");
        u1.setFullname("user");

        when(userdb.save(u1)).thenReturn(u1);
        // assertEquals(u1, userService.addUser(u1));
    }

    

    @Test
    void testDeleteByUsername() {
        List<User> l1 = new ArrayList<User>();
        User u2 = new User();
        User u1 = new User();
        u1.setUsername("user992");
        u1.setFullname("user");
        u2.setUsername("USER992");
        u2.setFullname("USER");
        l1.add(u1);
        l1.add(u2);
        // when(userdb.deleteAll()).thenReturn();
        // userService.addUser(u1);

        // when(userdb.deleteAll()).thenReturn("Deleted Successfully....");
        // assertEquals("Deleted Successfully....", userService.deleteByUsername("user992"));

    }

    @Test
    void testGetUsers() {
        List<User> l1 = new ArrayList<User>();
        User u2 = new User();
        User u1 = new User();
        u1.setUsername("user992");
        u1.setFullname("user");
        u2.setUsername("USER992");
        u2.setFullname("USER");
        l1.add(u1);
        l1.add(u2);
        when(userdb.findAll()).thenReturn(l1);
        assertEquals(2, userService.getUsers().size());

    }

    @Test
    void testUpadateFullname() {

    }
}
