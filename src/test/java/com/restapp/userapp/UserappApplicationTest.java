package com.restapp.userapp;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
// import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.client.WebClient;
import static org.mockito.BDDMockito.given;

import java.util.List;

import com.restapp.userapp.model.User;
import com.restapp.userapp.repo.UserRepo;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class UserappApplicationTest {
    @LocalServerPort
    private int port;

    @Autowired
    private WebClient.Builder webTestClient;

    @MockBean
    UserRepo db;

    User u1;
    
    @Before
    public void setup() {
        u1 = new User();
        u1.setUsername("user992");
        u1.setFullname("user");
        // System.out.println("in before");
        given(db.save(u1)).willReturn(u1);
        given(db.findAll()).willReturn(List.of(u1));
        given(db.findByUsername("user992")).willReturn(List.of(u1));
    }

    @After
    public void destroying() {
        // System.out.println(" in after each");
        u1 = null;
    }

    @Test
    public void testAddUser() {
        // assertNotNull(u1.getUsername());
        ResponseEntity<User> savedResponse = webTestClient.baseUrl("http://localhost:" + port)
                .build()
                .post()
                .uri("/users")
                .bodyValue(u1)
                .retrieve().toEntity(User.class).block();
        assertEquals(u1.getUsername(), savedResponse.getBody().getUsername());

    }

    @Test
    public void testGetUsers() {

        ResponseEntity<List<User>> savedResponse = webTestClient.baseUrl("http://localhost:" + port)
                .build()
                .get()
                .uri("/users")
                .retrieve().toEntityList(User.class).block();
        // System.out.println(savedResponse.getBody());
        assertEquals(savedResponse.getBody(), List.of(u1));
        // assertFalse(savedResponse.hasBody());
    }

    @Test
    public void testUpadateFullname() {

        ResponseEntity<User> savedResponse = webTestClient.baseUrl("http://localhost:" + port)
                .build()
                .put()
                .uri("/users/user992")
                .retrieve().toEntity(User.class).block();
        // System.out.println(savedResponse + "kkkkk");
        assertEquals(savedResponse.getBody().getFullname().length(), u1.getFullname().length());
        // assertFalse(savedResponse.hasBody());
    }

}
