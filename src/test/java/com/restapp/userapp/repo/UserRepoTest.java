package com.restapp.userapp.repo;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.restapp.userapp.model.User;

@SpringBootTest
public class UserRepoTest {
    @Autowired
    UserRepo db;

    @Test
    void testFindByUsername() {
        User u1 = new User();
        u1.setUsername("user992");
        u1.setFullname("user");
        u1 = db.save(u1);

        List<User> users = db.findByUsername("user992");
        assertEquals("user992", users.get(0).getUsername());

    }

    @AfterEach
    void tearDown() {
        db.deleteAll();
    }

}
