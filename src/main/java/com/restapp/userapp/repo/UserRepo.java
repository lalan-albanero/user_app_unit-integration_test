package com.restapp.userapp.repo;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.restapp.userapp.model.User;

@Repository
public interface UserRepo extends MongoRepository<User, String> {
    @Query
    List<User> findByUsername(String username);

}
