package com.restapp.userapp.model;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document
public class User {
    @Id
    private String _id;
    @NotNull
    @Indexed(unique = true)
    private String username;
    private String fullname;
    private String email;
    private String address;
    private long mobile;
    private String current_organization;
}
