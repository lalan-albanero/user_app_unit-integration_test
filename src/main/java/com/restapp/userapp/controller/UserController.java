package com.restapp.userapp.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.web.bind.annotation.RestController;

import com.restapp.userapp.model.User;
import com.restapp.userapp.repo.UserRepo;
import com.restapp.userapp.service.UserServiceImpl;

@RestController
public class UserController {
    @Autowired
    UserServiceImpl userService;

    UserRepo db;

    @GetMapping("/")
    ResponseEntity<?> test() {
        return ResponseEntity.ok("Good to go...");
    }

    @PostMapping("/users")
    ResponseEntity<?> addUser(@Valid @RequestBody User user) {
        // System.out.println("in adduser");
        User u1=userService.addUser(user);
        // System.out.println("in adduser after");
        return ResponseEntity.ok(u1);
    }

    @GetMapping("/users")
    ResponseEntity<?> getUser() {
        return ResponseEntity.ok(userService.getUsers());
    }

    @PutMapping("/users/{username}")
    ResponseEntity<?> updateFullname(@PathVariable("username") String username) {
        return ResponseEntity.ok(userService.upadateFullname(username));
    }

    @DeleteMapping("/users/{username}")
    ResponseEntity<?> deleteUser(@PathVariable("username") String username) {
        return ResponseEntity.ok(userService.deleteByUsername(username));
    }

}
