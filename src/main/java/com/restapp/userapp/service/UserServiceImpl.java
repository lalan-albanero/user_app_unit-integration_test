package com.restapp.userapp.service;

import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.restapp.userapp.model.User;
import com.restapp.userapp.repo.UserRepo;

@Service
public class UserServiceImpl {
    @Autowired
    UserRepo db;

    public UserServiceImpl(UserRepo u) {
        this.db = u;
    }

    public User addUser(User user)  {
        // System.out.println("in adduser service...");
        // String userName = user.getUsername();
        // if (!userName.isBlank()) {
        //     // throw new Exception("UserName is required.");
        // } 
            return db.save(user);
        

    }

    public List<User> getUsers() {
        return db.findAll();
    }

    public User upadateFullname(String username) {
        List<User> saved = db.findByUsername(username);
        String fullname = saved.get(0).getFullname();
        String newFullname = "";
        Random r = new Random();
        String vowels = "AEIOUaeiou";
        String specialChar = "<>@!#$%^&*()_+[]{}?:|'/~`-=";
        for (int i = 0; i < fullname.length(); i++) {
            char ele = fullname.charAt(i);
            if (vowels.indexOf(ele) >= 0) {
                newFullname += specialChar.charAt(r.nextInt(26 - 0) + 0);
            } else {
                newFullname += ele;
            }
        }
        saved.get(0).setFullname(newFullname);

        User u2 = db.save(saved.get(0));
        return u2;
    }

    public String deleteByUsername(String username) {
        List<User> saved = db.findByUsername(username);
        if (saved.size() > 0) {
            db.deleteAll(saved);
            return "Deleted Successfully....";

        } else {
            return "No Record found...";
        }
    }

}
